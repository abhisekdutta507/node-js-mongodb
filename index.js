const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
require('dotenv').config();
require('./model/').connect();

const Auth = require('./controller/auth');
const User = require('./controller/user');
const Car = require('./controller/car');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const api = express.Router();
const routes = () => {
  // route parameter
  api.post('/users', User.create);
  api.get('/users', User.findLimited);
  api.get('/users/:_id', User.findLimited);
  api.patch('/users/:_id', User.updateOne);
  api.delete('/users/:_id', User.deleteMany);

  api.post('/cars', Car.create);
  api.get('/cars', Car.findLimited);

  api.post('/login', Auth.login);
  api.get('/logout', Auth.logout);

  return api;
};

app.use('/api', routes());

const server = http.Server(app);
server.listen(process.env.PORT || 5000);
