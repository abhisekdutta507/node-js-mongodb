const Models = require('../model/').objects;

exports.findLimited = async function (req, res) {
  let params = req.query;
  const option = {
    sort: !isNaN(Number(params.sort)) ? Number(params.sort) : -1,
    skip: !isNaN(Number(params.skip)) ? Number(params.skip) : 0,
    limit: !isNaN(Number(params.limit)) ? Number(params.limit) : 10,
    select: params.select ? params.select.split(',') : [],
    populate: params.populate ? params.populate.split(',') : []
  };

  params = req.query.search ? {
    $and: [
      { isActive: true },
      {
        $or: [
          { name: { $regex: RegExp(`${req.query.search}`), $options: 'gi' } },
          { department: { $regex: RegExp(`${req.query.search}`), $options: 'gi' } }
        ]
      }
    ]
  } : {};

  const u = await Models.crudappuser.findLimited(req.params._id ? req.params : params, option);
  if (u.error) { return res.status(404).set('Content-Type', 'application/json').send(u.error); }

  const l = await Models.crudappuser.countDocuments(req.params._id ? req.params : params);
  u.available = l.data;

  if (req.params._id) { u.data = u.data[0]; }
  return res.status(200).send(u);
}

exports.create = async function (req, res) {
  // req.body = _.omit(req.body, ['id', 'email', 'photoPath']);
  /**
   * @description UPLOADS the image file on Cloudinary.
   */
  if (req.file) {
    let URL = (process.env.DEVELOPMENT ? `http://localhost:${process.env.PORT}/image/avatar/` : 'https://nodeapis101.herokuapp.com/image/avatar/') + req.file.filename;
    req.body.photoPath = URL;
  }

  const u = await Models.crudappuser.create(req.body);
  if (u.error) { return res.status(404).set('Content-Type', 'application/json').send(u.error); }
  return res.status(200).send(u);
}

exports.updateOne = async function (req, res) {
  // req.body = _.omit(req.body, ['_id', 'email']);
  /**
   * @description UPLOADS the image file on Cloudinary.
   */
  if (req.file) {
    let URL = (process.env.DEVELOPMENT ? `http://localhost:${process.env.PORT}/image/avatar/` : 'https://nodeapis101.herokuapp.com/image/avatar/') + req.file.filename;
    req.body.photoPath = URL;
  }

  const u = await Models.crudappuser.updateOne(req.params, req.body, {});
  if (u.error) { return res.status(404).set('Content-Type', 'application/json').send(u.error); }
  return res.status(200).send(u);
}

exports.deleteMany = async function (req, res) {
  const u = await Models.crudappuser.deleteMany(req.params);
  if (u.error) { return res.status(404).set('Content-Type', 'application/json').send(u.error); }
  return res.status(200).send(u);
}
