const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;
const env = require('../environment/');

var CrudAppUser, Car;

const Models = {
  connect: async () => {
    let mongoUrl = `mongodb://${env.Mlab.username}:${env.Mlab.password}@${env.Mlab.host}:${env.Mlab.port}/${env.Mlab.database}`;
    mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
    mongoose.set('useCreateIndex', true);
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', () => {
      // console.log(`MongoDB is available at mongodb://${env.Mlab.host}:${env.Mlab.port}/${env.Mlab.database}`);
      Models.create.crudappuser();
      Models.create.car();
    });

  },

  create: {
    crudappuser: async () => {
      let schema = new Schema({
        name: { type: String, required: true },
        dateOfBirth: { type: Date, required: true },
        department: { type: String, required: true },
        isActive: { type: Boolean, default: true },
        photoPath: { type: String },
        cars: [{ type: Schema.Types.ObjectId, ref: 'CrudAppCar' }]
      });
      CrudAppUser = mongoose.model('CrudAppUser', schema);
    },

    car: async () => {
      let schema = new Schema({
        name: { type: String, required: true },
        brand: { type: String, required: true },
        price: { type: Number, required: true },
        photoPath: { type: String }
      });
      Car = mongoose.model('CrudAppCar', schema);
    }
  },

  ObjectId: ObjectId,

  // request: request,

  objects: {

    crudappuser: {
      /**
       * @description Count total number of available documents.
       */
      countDocuments: async (query) => {
        let r;
        try { r = await CrudAppUser.countDocuments(query); }
        catch (e) { return { error: { message: { type: 'error', text: e.message } } }; }
        return { message: { type: 'success' }, data: r || 0 };
      },

      /**
       * @description finds limited of the available automobiles in the Mlab database.
       */
      findLimited: async (query, option) => {
        let r;
        try { r = await CrudAppUser.find(query).sort({ createdAt: option.sort }).skip(option.skip).limit(option.limit).select(option.select).populate(option.populate); }
        catch (e) { return { error: { message: { type: 'error', text: e.message } } }; }
        // if(!r.length) { return { error: { message: { type: 'error', text: 'no crudapp user found!' } } }; }
        return { message: { type: 'success' }, data: r };
      },

      /**
       * @description creates an automobile with required parameters.
       */
      create: async (param) => {
        let r;
        try { r = await CrudAppUser.create(param); }
        catch (e) { return { error: { message: { type: 'error', text: e.message } } }; }
        if (!r) { return { error: { message: { type: 'error', text: 'can\'t create crudapp user!' } } }; }
        return { message: { type: 'success' }, data: r };
      },

      /**
       * @description updates only one user at a time. If parameter contains password it updates jwtValidatedAt.
       */
      updateOne: async (query, param, option) => {
        let r;
        try { r = await CrudAppUser.updateOne(query, param, option); }
        catch (e) { { return { error: { message: { type: 'error', text: e.message } } }; } }
        if (!r.n) { return { error: { message: { type: 'error', text: 'crudapp user doesn\'t exists!' } } }; }
        return { message: { type: 'success' }, data: r };
      },

      /**
       * @description deletes only one user matching the parameters from Mlab database.
       */
      deleteMany: async (param) => {
        let r;
        try { r = await CrudAppUser.deleteMany(param); }
        catch (e) { return { error: { message: { type: 'error', text: e.message } } }; }
        if (!r.n) { return { error: { message: { type: 'error', text: 'crudapp user doesn\'t exists!' } } }; }
        return { message: { type: 'success' }, data: r };
      },
    },

    car: {
      /**
       * @description Count total number of available documents.
       */
      countDocuments: async (query) => {
        let r;
        try { r = await Car.countDocuments(query); }
        catch (e) { return { error: { message: { type: 'error', text: e.message } } }; }
        return { message: { type: 'success' }, data: r || 0 };
      },

      /**
       * @description finds limited of the available automobiles in the Mlab database.
       */
      findLimited: async (query, option) => {
        let r;
        try { r = await Car.find(query).sort({ createdAt: option.sort }).skip(option.skip).limit(option.limit).select(option.select).populate(option.populate); }
        catch (e) { return { error: { message: { type: 'error', text: e.message } } }; }
        // if(!r.length) { return { error: { message: { type: 'error', text: 'no crudapp user found!' } } }; }
        return { message: { type: 'success' }, data: r };
      },

      /**
       * @description creates an automobile with required parameters.
       */
      create: async (param) => {
        let r;
        try { r = await Car.create(param); }
        catch (e) { return { error: { message: { type: 'error', text: e.message } } }; }
        if (!r) { return { error: { message: { type: 'error', text: 'can\'t create car!' } } }; }
        return { message: { type: 'success' }, data: r };
      }
    }
  }
};

module.exports = Models;
